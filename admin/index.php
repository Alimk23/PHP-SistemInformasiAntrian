<?php 
    session_start();
    if (!isset($_SESSION['login'])) {
      $msg = "<p class='alert alert-danger'>Silahkan login terlebih dahulu </p>";
      header ("Location: ../index.php?msg=$msg");
    } 
    else if ($_SESSION['level']!=1) {
      $msg = "<p class='alert alert-danger'>Anda tidak berhak mengakses halaman ini</p>"; 
      echo $msg;
      header ("Location: ../index.php?msg=$msg");
    }
    $conn = mysqli_connect("localhost","root","","db_kel8b");
    
    //menampilkan nama pada sidebar
    $id = $_GET["id"];
    $querySidebar = "SELECT * FROM data_login WHERE id='$id'";
    $resultSidebar = mysqli_query($conn,$querySidebar);
    $dataSidebar = mysqli_fetch_assoc ($resultSidebar);

    // menampilkan data jumlah pengguna
    $queryLogin = "SELECT * FROM data_login";
    $dataLogin = mysqli_query($conn,$queryLogin);
    $a=0;
    foreach ($dataLogin as $jumlahPengguna) {
        $a++;
    }  
    
    // menampilkan jumlah dokter
    $queryDokter = "SELECT * FROM data_dokter";
    $dataDokter = mysqli_query($conn,$queryDokter);
    $b=0;
    foreach ($dataDokter as $jumlahDokter) {
        $b++;
    }
    // menampilkan data jumlah antrian
    $queryAntrian = "SELECT * FROM data_antrian";
    $dataAntrian = mysqli_query($conn,$queryAntrian);
    $c=0;
    foreach ($dataAntrian as $JumlahAntrian) {
        $c++;
    }  
    
    // menampilkan jumlah validasi antrian
    $queryValidasi = "SELECT * FROM data_validasi";
    $dataValidasi = mysqli_query($conn,$queryValidasi);
    $d=0;
    foreach ($dataValidasi as $jumlahValidasi) {
        $d++;
    }
    
    // menampilkan nomor antrian yang berjalan
    $queryQrun = "SELECT nomor FROM data_antrian";
    $dataQ = mysqli_query($conn,$queryQrun);
    $dataQrun = mysqli_fetch_assoc($dataQ);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel=”icon” href=”template/logo.png”>
    <title>Admin Dashboard</title>
    
    <!-- Custom fonts for this template-->
    <link href="../template/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />

    <!-- Custom styles for this template-->
    <link href="../template/css/sb-admin-2.min.css" rel="stylesheet" /></head>
<body>
    <!-- Page Wrapper -->
    <div id="wrapper">
      <!-- Sidebar -->
      <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php?id=<?=$dataSidebar['id']?>">
          <div class="sidebar-brand-icon">
            <i class="fas fa-user-circle"></i>
          </div>
          <div class="sidebar-brand-text mx-3">
            <?php
              echo $dataSidebar["nama"];
            ?>
          </div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0" />

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
          <a class="nav-link" href="index.php?id=<?=$dataSidebar['id']?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Nav Item - Data Pengguna -->
        <li class="nav-item">
          <a class="nav-link" href="data-pengguna.php?id=<?=$dataSidebar['id']?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Data Pengguna</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider" />
        <!-- Nav Item - Data Dokter -->
        <li class="nav-item">
          <a class="nav-link" href="data-dokter.php?id=<?=$dataSidebar['id']?>">
            <i class="fas fa-fw fa-stethoscope"></i>
            <span>Data Dokter</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Nav Item - Data Antrian -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-history fa-cog"></i>
            <span>Data Antrian</span>
          </a>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="buka-sesi.php?id=<?=$dataSidebar['id']?>">Buka Sesi Antrian</a>
              <a class="collapse-item" href="validasi.php?id=<?=$dataSidebar['id']?>">Validasi Antrian</a>
              <a class="collapse-item" href="data-antrian.php?id=<?=$dataSidebar['id']?>">Total Antrian</a>
            </div>
          </div>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Nav Item - Logout -->
        <li class="nav-item">
          <a class="nav-link" href="../logout.php">
            <i class="fas fa-fw fa-sign-out-alt"></i>
            <span>Logout</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block" />

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
          <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>
      </ul>
      <!-- End of Sidebar -->

      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
          <!-- Topbar -->
          <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
            <!-- Sidebar Toggle (Topbar) -->
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
              <i class="fa fa-bars"></i>
            </button>

            <!-- Header -->
            <div class="mr-auto ml-md-2 my-2 my-md-0 mw-100 pt-2">
              <h5>Sistem Pendaftaran Poliklinik</h5>
            </div>
          </nav>
          <!-- End of Topbar -->

          <!-- Begin Page Content -->
          <div class="container-fluid">
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
              <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            </div>

            <!-- Content Row -->
            <div class="row">
              <!-- Jumlah Pengguna -->
              <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Pengguna</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$a?></div>
                      </div>
                      <div class="col-auto">
                        <a href="data-pengguna.php?id=<?=$dataSidebar['id']?>">                        
                          <i class="fas fa-users fa-3x text-gray-300"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Jumlah Dokter -->
              <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-info shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Jumlah Dokter</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$b?></div>
                      </div>
                      <div class="col-auto">
                        <a href="data-dokter.php?id=<?=$dataSidebar['id']?>">                        
                          <i class="fas fa-stethoscope fa-3x text-gray-300"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Jumlah Antrian -->
              <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah Antrian</div>
                        <div class="row no-gutters align-items-center">
                          <div class="col-auto">
                            <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?=$c?></div>
                          </div>
                        </div>
                      </div>
                      <div class="col-auto">
                        <a href="data-antrian.php?id=<?=$dataSidebar['id']?>">                        
                          <i class="fas fa-history fa-3x text-gray-300"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Validasi Antrian -->
              <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Validasi Antrian</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$d?></div>
                      </div>
                      <div class="col-auto">
                        <a href="validasi.php?id=<?=$dataSidebar['id']?>">
                          <i class="fas fa-bell fa-3x text-gray-300"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="row justify-content-center">
              <!-- Pie Chart -->
              <div class="col-xl-4 col-lg-5">
                <div class="card shadow mb-4">
                  <!-- Card Header - Dropdown -->
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Antrian Berjalan</h6>
                  </div>
                  <!-- Card Body -->
                  <div class="card-body">
                    <div class="mt-2 text-center">
                      <h1 id="nextQ" class="mr-2 font-weight-bold text-success" style="font-size: 60px;"> 
                        <?php
                          if (!empty($dataQrun)){
                            $loop=0;
                            foreach ($dataQrun as $dq) {
                                $loop++;
                            }           
                            echo $dq;
                          }
                          else {
                            echo 0;
                          }
                        ?>
                      </h1>
                      <form method="POST" action="../aksi.php" name="form-nextQ">
                        <div class="form-group row d-none">
                          <div class="col-sm-6 mb-3 mb-sm-0">
                            <input name="idSidebar" value="<?=$dataSidebar['id']?>"/>
                          </div>
                        </div>
                        <div class="form-group row d-none">
                          <div class="col-sm-6 mb-3 mb-sm-0">
                            <input name="nextQ" value="<?=$dq?>"/>
                          </div>
                        </div>
                        <button type="submit" class="btn bg-gradient-primary text-white" name="btn-nextQ">Antrian Selanjutnya</button>
                      </form>                  
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright &copy; Alim Mughanil Karim & M. Dhiyaul Allam</span>
            </div>
          </div>
        </footer>
        <!-- End of Footer -->
      </div>
      <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->

    <!-- Bootstrap core JavaScript-->
    <script src="../template/vendor/jquery/jquery.min.js"></script>
    <script src="../template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../template/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../template/js/sb-admin-2.min.js"></script>
</body>
</html>