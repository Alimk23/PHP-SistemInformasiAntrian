<?php 
    session_start();
    if (!isset($_SESSION['login'])) {
      $msg = "<p class='alert alert-danger'>Silahkan login terlebih dahulu </p>";
      header ("Location: ../index.php?msg=$msg");
      exit;
    } 
    else if ($_SESSION['level']!=1) {
      $msg = "Anda tidak berhak mengakses halaman ini";
      echo $msg;
      header ("Location: ../index.php?msg=$msg");
      exit;
    }
    
    $conn = mysqli_connect("localhost","root","","db_kel8b");
    $query = "SELECT * FROM data_validasi";
    $result = mysqli_query($conn,$query);
    $data = mysqli_fetch_assoc($result);
    
    //menampilkan nama pada sidebar
    $id = $_GET["id"];
    $querySidebar = "SELECT * FROM data_login WHERE id='$id'";
    $resultSidebar = mysqli_query($conn,$querySidebar);
    $dataSidebar = mysqli_fetch_assoc ($resultSidebar);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Validasi Antrian</title>
    
    <!-- Custom fonts for this template-->
    <link href="../template/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />

    <!-- Custom styles for this template-->
    <link href="../template/css/sb-admin-2.min.css" rel="stylesheet" />
    <!-- Custom styles for this page -->
    <link href="../template/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" />
</head>
<body>
    <!-- Page Wrapper -->
    <div id="wrapper">
      <!-- Sidebar -->
      <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php?id=<?=$dataSidebar['id']?>">
          <div class="sidebar-brand-icon">
            <i class="fas fa-user-circle"></i>
          </div>
          <div class="sidebar-brand-text mx-3">
            <?php
              echo $dataSidebar["nama"];
            ?>
          </div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0" />

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
          <a class="nav-link" href="index.php?id=<?=$dataSidebar['id']?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Nav Item - Data Pengguna -->
        <li class="nav-item">
          <a class="nav-link" href="data-pengguna.php?id=<?=$dataSidebar['id']?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Data Pengguna</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider" />
        <!-- Nav Item - Data Dokter -->
        <li class="nav-item">
          <a class="nav-link" href="data-dokter.php?id=<?=$dataSidebar['id']?>">
            <i class="fas fa-fw fa-stethoscope"></i>
            <span>Data Dokter</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Nav Item - Data Antrian -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-history fa-cog"></i>
            <span>Data Antrian</span>
          </a>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="buka-sesi.php?id=<?=$dataSidebar['id']?>">Buka Sesi Antrian</a>
              <a class="collapse-item" href="validasi.php?id=<?=$dataSidebar['id']?>">Validasi Antrian</a>
              <a class="collapse-item" href="data-antrian.php?id=<?=$dataSidebar['id']?>">Total Antrian</a>
            </div>
          </div>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Nav Item - Logout -->
        <li class="nav-item">
          <a class="nav-link" href="../logout.php">
            <i class="fas fa-fw fa-sign-out-alt"></i>
            <span>Logout</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block" />

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
          <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>
      </ul>      
      <!-- End of Sidebar -->

      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
          <!-- Topbar -->
          <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
            <!-- Sidebar Toggle (Topbar) -->
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
              <i class="fa fa-bars"></i>
            </button>

            <!-- Header -->
            <div class="mr-auto ml-md-2 my-2 my-md-0 mw-100 pt-2">
              <h5>Sistem Pendaftaran Poliklinik</h5>
            </div>
          </nav>
          <!-- End of Topbar -->

          <!-- Begin Page Content -->
          <div class="container-fluid">
            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">Menunggu Validasi</h1>
            <h6 class="msg"><?php if(isset($_GET['msg'])) echo $_GET['msg']?></h6>
            <div class="card shadow mb-4">
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Jadwal</th>
                          <th>Waktu</th>
                          <th>Klinik</th>
                          <th>Kuota</th>
                          <th>Sisa</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $i=0;
                          foreach ($result as $data) {
                              $i++;
                        ?>
                        <tr>
                          <td>
                            <?php
                                echo $i;
                            ?>
                          </td>
                          <td>
                            <?php
                                echo $data["nama"];
                            ?>
                          </td>
                          <td>
                            <?php
                                echo $data["jadwal"];
                            ?>
                          </td>
                          <td>
                            <?php
                                echo $data["waktu"];
                            ?>
                          </td>
                          <td>
                            <?php
                                echo $data["klinik"];
                            ?>
                          </td>
                          <td>
                            <?php
                                echo $data["kuota"];
                            ?>
                          </td>
                          <td>
                            <?php
                                echo $data["sisa"];
                            ?>
                          </td>
                          <td>
                            <button type="button" class="btn btn-primary pt-0 pb-0" data-toggle="modal" data-target="#modalValidasi">Validasi</button>
                            <button type="button" class="btn btn-danger pt-0 pb-0">
                              <a href="../aksi.php?delValidasi&idVal=<?=$data['id']?>&idJadwal=<?=$data['idJadwal']?>&idSidebar=<?=$dataSidebar['id']?>" class="text-light text-decoration-none">Hapus</a>
                            </button>
                            <form method="POST" action="../aksi.php" name="input-nomor">
                              <div class="modal fade" id="modalValidasi" tabindex="-1" aria-labelledby="modalValidasiLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="modalValidasiLabel">Input Nomor</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">         
                                          <div class="form-group row d-none">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                              <input name="id" value="<?=$data['id']?>"/>
                                            </div>
                                          </div>
                                          <div class="form-group row d-none">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                              <input name="idUser" value="<?=$data['idUser']?>"/>
                                            </div>
                                          </div>
                                          <div class="form-group row d-none">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                              <input name="idSidebar" value="<?=$dataSidebar['id']?>"/>
                                            </div>
                                          </div>
                                          <div class="form-group row d-none">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                              <input name="jadwal" value="<?=$data['jadwal']?>"/>
                                            </div>
                                          </div>
                                          <div class="form-group row d-none">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                              <input name="waktu" value="<?=$data['waktu']?>"/>
                                            </div>
                                          </div>
                                          <div class="form-group row d-none">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                              <input name="nama" value="<?=$data['nama']?>"/>
                                            </div>
                                          </div>
                                          <div class="form-group row d-none">
                                            <div class="col-sm-6 mb-3 mb-sm-0">
                                              <input name="klinik" value="<?=$data['klinik']?>"/>
                                            </div>
                                          </div>
                                          <div class="form-group row">
                                            <div class="col-6 mb-3 mb-sm-0">
                                              <label for="nomorAntrian">Masukan Nomor Antrian</label>
                                              <input id="nomorAntrian" name="nomorAntrian" required="required"/>
                                            </div>
                                          </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                      <button type="submit" class="btn btn-primary" name="validasi">Kirim</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </td>
                          </tr>
                        <?php
                          }
                        ?>                      
                      </tbody>
                  </table>
                </div>
              </div>
            </div>            
          </div>
          <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright &copy; Alim Mughanil Karim & M. Dhiyaul Allam</span>
            </div>
          </div>
        </footer>
        <!-- End of Footer -->
      </div>
      <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->

    <!-- Bootstrap core JavaScript-->
    <script src="../template/vendor/jquery/jquery.min.js"></script>
    <script src="../template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../template/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../template/js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="../template/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="../template/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="../template/js/demo/datatables-demo.js"></script>
    <script src="../aksi.js"></script>
</body>
</html>