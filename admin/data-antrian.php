<?php 
    session_start();
    if (!isset($_SESSION['login'])) {
      $msg = "<p class='alert alert-danger'>Silahkan login terlebih dahulu </p>";
      header ("Location: ../index.php?msg=$msg");
      exit;
    } 
    else if ($_SESSION['level']!=1) {
      $msg = "Anda tidak berhak mengakses halaman ini";
      echo $msg;
      header ("Location: ../index.php?msg=$msg");
      exit;
    }
    
    $conn = mysqli_connect("localhost","root","","db_kel8b");
    $query = "SELECT * FROM data_antrian";
    $result = mysqli_query($conn,$query);
    $data = mysqli_fetch_assoc($result);
    
    //menampilkan nama pada sidebar
    $id = $_GET["id"];
    $querySidebar = "SELECT * FROM data_login WHERE id='$id'";
    $resultSidebar = mysqli_query($conn,$querySidebar);
    $dataSidebar = mysqli_fetch_assoc ($resultSidebar);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Antrian</title>
    
    <!-- Custom fonts for this template-->
    <link href="../template/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />

    <!-- Custom styles for this template-->
    <link href="../template/css/sb-admin-2.min.css" rel="stylesheet" />
    <!-- Custom styles for this page -->
    <link href="../template/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" />
</head>
<body>
    <!-- Page Wrapper -->
    <div id="wrapper">
      <!-- Sidebar -->
      <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php?id=<?=$dataSidebar['id']?>">
          <div class="sidebar-brand-icon">
            <i class="fas fa-user-circle"></i>
          </div>
          <div class="sidebar-brand-text mx-3">
            <?php
              echo $dataSidebar["nama"];
            ?>
          </div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0" />

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
          <a class="nav-link" href="index.php?id=<?=$dataSidebar['id']?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Nav Item - Data Pengguna -->
        <li class="nav-item">
          <a class="nav-link" href="data-pengguna.php?id=<?=$dataSidebar['id']?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Data Pengguna</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider" />
        <!-- Nav Item - Data Dokter -->
        <li class="nav-item">
          <a class="nav-link" href="data-dokter.php?id=<?=$dataSidebar['id']?>">
            <i class="fas fa-fw fa-stethoscope"></i>
            <span>Data Dokter</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Nav Item - Data Antrian -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-history fa-cog"></i>
            <span>Data Antrian</span>
          </a>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="buka-sesi.php?id=<?=$dataSidebar['id']?>">Buka Sesi Antrian</a>
              <a class="collapse-item" href="validasi.php?id=<?=$dataSidebar['id']?>">Validasi Antrian</a>
              <a class="collapse-item" href="data-antrian.php?id=<?=$dataSidebar['id']?>">Total Antrian</a>
            </div>
          </div>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Nav Item - Logout -->
        <li class="nav-item">
          <a class="nav-link" href="../logout.php">
            <i class="fas fa-fw fa-sign-out-alt"></i>
            <span>Logout</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block" />

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
          <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>
      </ul>      
      <!-- End of Sidebar -->

      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
          <!-- Topbar -->
          <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
            <!-- Sidebar Toggle (Topbar) -->
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
              <i class="fa fa-bars"></i>
            </button>

            <!-- Header -->
            <div class="mr-auto ml-md-2 my-2 my-md-0 mw-100 pt-2">
              <h5>Sistem Pendaftaran Poliklinik</h5>
            </div>
          </nav>
          <!-- End of Topbar -->

          <!-- Begin Page Content -->
          <div class="container-fluid">
            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">Data Antrian Berjalan</h1>
            <h6 class="msg"><?php if(isset($_GET['msg'])) echo $_GET['msg']?></h6>
            <div class="card shadow mb-4">
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Jadwal</th>
                          <th>Waktu</th>
                          <th>Nama</th>
                          <th>Klinik</th>
                          <th>Nomor Antrian</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $i=0;
                          foreach ($result as $data) {
                              $i++;
                        ?>
                        <tr>
                          <td>
                            <?php
                                echo $i;
                            ?>
                          </td>
                          <td>
                            <?php
                                echo $data["jadwal"];
                            ?>
                          </td>
                          <td>
                            <?php
                                echo $data["waktu"];
                            ?>
                          </td>
                          <td>
                            <?php
                                echo $data["nama"];
                            ?>
                          </td>
                          <td>
                            <?php
                                echo $data["klinik"];
                            ?>
                          </td>
                          <td>
                            <?php
                                echo $data["nomor"];
                            ?>
                          </td>
                          <td>
                            <button type="button" class="btn btn-primary pt-0 pb-0 btnDetail" data-toggle="modal" data-target="#modalDetail" data-id="<?= $data['id'];?>">Detail</button>
                            <form>
                              <div class="modal fade" id="modalDetail" tabindex="-1" aria-labelledby="modalDetailLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="modalDetailLabel">Data Diri User</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <!-- input nama -->
                                      <label for="nama" class="col-sm col-form-label">Nama</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <input type="text" class="form-control" id="nama" name="nama">
                                          </div>
                                      </div>
                                      <!-- input tempat, tanggal lahir -->
                                      <label for="ttl" class="col-sm col-form-label">Tempat dan Tanggal Lahir</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <input type="text" class="form-control" id="ttlTempat" name="ttlTempat">
                                          </div>
                                          <div class="col-sm">
                                              <input type="date" class="form-control" id="ttlTanggal" name="ttlTanggal">
                                          </div>
                                      </div>
                                      <!-- input gender -->
                                      <label for="gender" class="col-sm col-form-label">Jenis Kelamin</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <input type="text" class="form-control" id="gender" name="gender">
                                          </div>
                                      </div>
                                      <!-- input kewarganegaraan -->
                                      <label for="kewarganegaraan" class="col-sm col-form-label">Kewarganegaraan</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <input type="text" class="form-control" id="kewarganegaraan" name="kewarganegaraan">
                                          </div>
                                      </div>
                                      <!-- input pendidikan -->
                                      <label for="pendidikan" class="col-sm col-form-label">Pendidikan</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <input type="text" class="form-control" id="pendidikan" name="pendidikan">
                                          </div>
                                      </div>
                                      <!-- input agama -->
                                      <label for="agama" class="col-sm col-form-label">Agama</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <input type="text" class="form-control" id="agama" name="agama">
                                          </div>
                                      </div>
                                      <!-- input pekerjaan -->
                                      <label for="pekerjaan" class="col-sm col-form-label">Pekerjaan</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <input type="text" class="form-control" id="pekerjaan" name="pekerjaan">
                                          </div>
                                      </div>
                                      <!-- input asuransi -->
                                      <label for="asuransi" class="col-sm col-form-label">Asuransi</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <input type="text" class="form-control" id="asuransi" name="asuransi">
                                          </div>
                                      </div>
                                      <!-- input alamat -->
                                      <label for="alamat" class="col-sm col-form-label">Alamat</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <div class="form-floating">
                                                  <textarea class="form-control pt-2" id="alamat" name="alamat"></textarea>
                                              </div>
                                          </div>
                                      </div>
                                      <!-- input nomor hp -->
                                      <label for="noHP" class="col-sm col-form-label">Nomor HP</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <input type="number" class="form-control" id="noHP" name="noHP">
                                          </div>
                                      </div>
                                      <!-- input email -->
                                      <label for="email" class="col-sm col-form-label">Email</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <input type="text" class="form-control" id="email" name="email">
                                          </div>
                                      </div>
                                      <!-- input nama wali -->
                                      <label for="namaWali" class="col-sm col-form-label">Nama Wali</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <input type="text" class="form-control" id="namaWali" name="namaWali">
                                          </div>
                                      </div>
                                      <!-- input hubungan wali dengan pasien -->
                                      <label for="hubunganWali" class="col-sm col-form-label">Hubungan dengan Pasien</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <input type="text" class="form-control" id="hubunganWali" name="hubunganWali">
                                          </div>
                                      </div>
                                      <!-- input alamat wali -->
                                      <label for="alamatWali" class="col-sm col-form-label">Alamat Wali</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <div class="form-floating">
                                                  <textarea class="form-control pt-2" id="alamatWali" name="alamatWali"></textarea>
                                              </div>
                                          </div>
                                      </div>
                                      <!-- input nomor hp wali-->
                                      <label for="noHPwali" class="col-sm col-form-label">No HP Wali</label>
                                      <div class="row mb-3">
                                          <div class="col-sm">
                                              <input type="number" class="form-control" id="noHPwali" name="noHPwali">
                                          </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </td>
                          </tr>
                        <?php
                          }
                        ?>                      
                      </tbody>
                  </table>
                </div>
              </div>
            </div>            
          </div>
          <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright &copy; Alim Mughanil Karim & M. Dhiyaul Allam</span>
            </div>
          </div>
        </footer>
        <!-- End of Footer -->
      </div>
      <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->

    <!-- Bootstrap core JavaScript-->
    <script src="../template/vendor/jquery/jquery.min.js"></script>
    <script src="../template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../template/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../template/js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="../template/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="../template/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="../template/js/demo/datatables-demo.js"></script>
    <script src="../aksi.js"></script>
</body>
</html>