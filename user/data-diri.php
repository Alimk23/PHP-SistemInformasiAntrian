<?php 
    session_start();    
    $conn = mysqli_connect("localhost","root","","db_kel8b");
    if (!isset($_SESSION['login'])) {
      $msg = "<p class='alert alert-danger'>Silahkan login terlebih dahulu</p>";
      header("Location: ../index.php?msg=$msg");
      exit;
    }
    //menampilkan nama pada sidebar
    $id = $_GET["id"];
    $querySidebar = "SELECT * FROM data_login WHERE id='$id'";
    $resultSidebar = mysqli_query($conn,$querySidebar);
    $dataSidebar = mysqli_fetch_assoc ($resultSidebar);

    $query = "SELECT * FROM data_diri WHERE id='$id'";
    $result = mysqli_query($conn,$query);
    $data = mysqli_fetch_assoc ($result);
    $idUser = $dataSidebar['id'];
    if (!isset($data)) {
        $msg = "<p class='alert alert-danger'>Silahkan isi data diri terlebih dahulu</p>";
        header("Location: isi-data-diri.php?id=$idUser&msg=$msg");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Diri</title>
    
    <!-- Custom fonts for this template-->
    <link href="../template/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />

    <!-- Custom styles for this template-->
    <link href="../template/css/sb-admin-2.min.css" rel="stylesheet" />
    <!-- Custom styles for this page -->
    <link href="../template/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" />
</head>
<body>
    <!-- Page Wrapper -->
    <div id="wrapper">
      <!-- Sidebar -->
      <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php?id=<?=$dataSidebar['id']?>">
          <div class="sidebar-brand-icon">
            <i class="fas fa-user-circle"></i>
          </div>
          <div class="sidebar-brand-text mx-3">
            <?php
              echo $dataSidebar["nama"];
            ?>
          </div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0" />

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
          <a class="nav-link" href="index.php?id=<?=$dataSidebar['id']?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Nav Item - Data Diri -->
        <li class="nav-item">
          <a class="nav-link" href="data-diri.php?id=<?=$dataSidebar['id']?>">
            <i class="fas fa-fw fa-users"></i>
            <span>Data Diri</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider" />
        <!-- Nav Item - Data Dokter -->
        <li class="nav-item">
          <a class="nav-link" href="data-dokter.php?id=<?=$dataSidebar['id']?>">
            <i class="fas fa-fw fa-stethoscope"></i>
            <span>Data Dokter</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Nav Item - Data Antrian -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-history fa-cog"></i>
            <span>Data Antrian</span>
          </a>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="pendaftaran-antrian.php?id=<?=$dataSidebar['id']?>">Pendaftaran Antrian</a>
              <a class="collapse-item" href="status-antrian.php?id=<?=$dataSidebar['id']?>">Status Antrian</a>
            </div>
          </div>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Nav Item - Logout -->
        <li class="nav-item">
          <a class="nav-link" href="../logout.php">
            <i class="fas fa-fw fa-sign-out-alt"></i>
            <span>Logout</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block" />

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
          <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>
      </ul>
      <!-- End of Sidebar -->

    <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
          <!-- Topbar -->
          <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
            <!-- Sidebar Toggle (Topbar) -->
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
              <i class="fa fa-bars"></i>
            </button>

            <!-- Header -->
            <div class="mr-auto ml-md-2 my-2 my-md-0 mw-100 pt-2">
              <h5>Sistem Pendaftaran Poliklinik</h5>
            </div>
          </nav>
          <!-- End of Topbar -->

          <!-- Begin Page Content -->
          <div class="container-fluid">
            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">Data Diri</h1>
            <div class="container">
                <div class="card shadow-sm">
                    <div class="card-header">
                        Data Diri
                    </div>
                    <h6 class="msg"><?php if(isset($_GET['msg'])) echo $_GET['msg']?></h6>
                    <div class="card-body">
                        <form action="../aksi.php" method="POST">
                            <div class="row mb-3 d-none">
                                <label for="idSidebar" class="col-sm-2 col-form-label">id</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="idSidebar" name="idSidebar" value="<?=$dataSidebar['id']?>">
                                </div>
                            </div>
                            <!-- input nomor rekam medis -->
                            <div class="row mb-3 d-none">
                                <label for="noRM" class="col-sm-2 col-form-label">No. Rekam Medis</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="noRM" name="noRM">
                                </div>
                            </div>
                            <!-- input nama -->
                            <div class="row mb-3">
                                <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="nama" name="nama" value="<?= $dataSidebar["nama"]; ?>" readonly>
                                </div>
                            </div>
                            <!-- input tempat, tanggal lahir -->
                            <div class="row mb-3">
                                <label for="ttl" class="col-sm-2 col-form-label">TTL</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="ttl" name="ttlTempat" value="<?= $data["ttlTempat"]; ?>" readonly>
                                </div>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" id="ttl" name="ttlTanggal" value="<?= $data["ttlTanggal"]; ?>" readonly>
                                </div>
                            </div>
                            <!-- input gender -->
                            <div class="row mb-3">
                                <label for="gender" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="gender" name="gender" value="<?= $data["gender"]; ?>" readonly>
                                </div>
                            </div>
                            <!-- input kewarganegaraan -->
                            <div class="row mb-3">
                                <label for="kewarganegaraan" class="col-sm-2 col-form-label">Kewarganegaraan</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="kewarganegaraan" name="kewarganegaraan" value="<?= $data["kewarganegaraan"]; ?>" readonly>
                                </div>
                            </div>
                            <!-- input pendidikan -->
                            <div class="row mb-3">
                                <label for="pendidikan" class="col-sm-2 col-form-label">Pendidikan</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="pendidikan" name="pendidikan" value="<?= $data["pendidikan"]; ?>" readonly>
                                </div>
                            </div>
                            <!-- input agama -->
                            <div class="row mb-3">
                                <label for="agama" class="col-sm-2 col-form-label">Agama</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="agama" name="agama" value="<?= $data["agama"]; ?>" readonly>
                                </div>
                            </div>
                            <!-- input pekerjaan -->
                            <div class="row mb-3">
                                <label for="pekerjaan" class="col-sm-2 col-form-label">Pekerjaan</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" value="<?= $data["pekerjaan"]; ?>" readonly>
                                </div>
                            </div>
                            <!-- input asuransi -->
                            <div class="row mb-3">
                                <label for="asuransi" class="col-sm-2 col-form-label">Asuransi</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="asuransi" name="asuransi" value="<?= $data["asuransi"]; ?>" readonly>
                                </div>
                            </div>
                            <!-- input alamat -->
                            <div class="row mb-3">
                                <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                                <div class="col-sm-6">
                                    <div class="form-floating">
                                        <textarea class="form-control pt-2" id="alamat" name="alamat" placeholder="<?= $data["alamat"]; ?>" readonly></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- input nomor hp -->
                            <div class="row mb-3">
                                <label for="noHP" class="col-sm-2 col-form-label">Nomor HP</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="noHP" name="noHP" value="<?= $data["noHP"]; ?>" readonly>
                                </div>
                            </div>
                            <!-- input email -->
                            <div class="row mb-3">
                                <label for="email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="email" name="email" value="<?= $data["email"]; ?>" readonly>
                                </div>
                            </div>
                            <div class="row pt-3 mt-1 mb-2 border">
                                <p class="ml-2">Isi data dibawah jika pasien berusia < 17 tahun </p>
                            </div>
                            <!-- input nama wali -->
                            <div class="row mb-3">
                                <label for="namaWali" class="col-sm-2 col-form-label">Nama Wali</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="namaWali" name="namaWali" value="<?= $data["namaWali"]; ?>" readonly>
                                </div>
                            </div>
                            <!-- input hubungan wali dengan pasien -->
                            <div class="row mb-3">
                                <label for="hubunganWali" class="col-sm-2 col-form-label">Hubungan dengan Pasien</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="hubunganWali" name="hubunganWali" value="<?= $data["hubunganWali"]; ?>" readonly>
                                </div>
                            </div>
                            <!-- input alamat wali -->
                            <div class="row mb-3">
                                <label for="alamatWali" class="col-sm-2 col-form-label">Alamat Wali</label>
                                <div class="col-sm-6">
                                    <div class="form-floating">
                                        <textarea class="form-control pt-2" id="alamatWali" name="alamatWali" placeholder="<?= $data["alamatWali"]; ?>" readonly></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- input nomor hp wali-->
                            <div class="row mb-3">
                                <label for="noHPwali" class="col-sm-2 col-form-label">No HP Wali</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="noHPwali" name="noHPwali" value="<?= $data["noHPwali"]; ?>" readonly>
                                </div>
                            </div>
                            <div class="row float-right mr-5">
                                <div class="col">
                                    <button type="button" class="btn btn-primary"><a href="edit-data-diri.php?id=<?= $dataSidebar["id"]; ?>" class="text-decoration-none text-light">Edit</a></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>    
          </div>
          <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright &copy; Alim Mughanil Karim & M. Dhiyaul Allam</span>
            </div>
          </div>
        </footer>
        <!-- End of Footer -->
      </div>
      <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->

    <!-- Bootstrap core JavaScript-->
    <script src="../template/vendor/jquery/jquery.min.js"></script>
    <script src="../template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../template/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../template/js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="../template/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="../template/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="../template/js/demo/datatables-demo.js"></script>
</body>
</html>