<?php
    session_start();
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $conn = mysqli_connect("localhost","root","","db_kel8b");
    //login
    $query = "select * from data_login where username='$username'";
    $login = mysqli_query($conn,$query);
    $isLogin = mysqli_num_rows($login);
    if (isset($_POST['login'])) {
        if ($isLogin>0) {
            $data = mysqli_fetch_assoc($login);
            $_SESSION['user'] = $username;
            if (password_verify ($password,$data["password"])) {
                if ($data['level']==1) {
                    $_SESSION['level'] = $data['level'];
                    $_SESSION['id'] = $data['id'];
                    $id = $_SESSION['id'];
                    $_SESSION['login']=true;
                    header("Location: admin/index.php?id=$id");
                    exit;
                }
                elseif ($data['level']==2) {
                    $_SESSION['level'] = $data['level'];
                    $_SESSION['id'] = $data['id'];
                    $id = $_SESSION['id'];
                    $_SESSION['login']=true;
                    header("Location: user/index.php?id=$id");
                    exit;
                }
            }
            else {
                $msg = "<p class='alert alert-danger'>Password salah. Silahkan login kembali</p>";
                header("Location: index.php?msg=$msg");
                exit;
            }
        }
        else {
            $msg = "<p class='alert alert-danger'>Username salah atau belum terdaftar</p>";
            header("Location: index.php?msg=$msg");
            exit;
        }
    }
    // register akun
    else if (isset($_POST['register'])) {
        $username = $_POST['username'];
        $nama = $_POST['nama'];
        $password1 = $_POST['password1'];
        $password2 = $_POST['password2'];
        if ($password1 != $password2) {
            $msg = "<p class='alert alert-danger'>Registrasi gagal. Password yang dimasukkan tidak sama</p>".mysqli_error($conn);
            header("Location: register.php?msg=$msg");
        }
        else {
            $queryCheck = "select * from data_login where username='$username'";
            $resultCheck = mysqli_query($conn,$queryCheck);
            $cekUsername = mysqli_fetch_assoc($resultCheck);
            if ($cekUsername["username"]==$username) {
                $msg = "<p class='alert alert-danger'>Registrasi gagal. Username telah terdaftar sebelumnya, silahkan login atau reset password.</p>".mysqli_error($conn);
                header("Location: index.php?msg=$msg");    
            }
            else {
                $password1 = password_hash($password1, PASSWORD_DEFAULT);
                $query = "insert into data_login (username,nama,password,level) values ('$username','$nama','$password','2')";
                $register = mysqli_query($conn,$query);
        
                if ($register == 'true') {
                    $msg = "<p class='alert alert-success'>Registrasi berhasil. Silahkan login</p>";
                    header("Location: index.php?msg=$msg");
                } 
                else {
                    $msg = "<p class='alert alert-danger'>Registrasi gagal</p>".mysqli_error($conn);
                    header("Location: registrasi.php?msg=$msg");
                }
            }
        }
    }
    // hapus data login
    else if (isset($_GET['del'])) {
        $idSidebar = $_GET['idSidebar'];
        $id = $_GET['id'];
        $query = "delete from data_login where id=$id";
    
        if (mysqli_query ($conn,$query) == 'true') {
            $msg = "<p class='alert alert-success'>Data berhasil dihapus</p>";
            header("Location: admin/data-pengguna.php?id=$idSidebar&msg=$msg");
        } 
        else {
            $msg = "<p class='alert alert-danger'>Data gagal dihapus</p>".mysqli_error($conn);
            header("Location: admin/data-pengguna.php?id=$idSidebar&msg=$msg");
        }
    }
    // tambah data dokter
    else if (isset($_POST['add-data'])) {
        $id = $_POST['id'];
        $nama = $_POST['nama'];
        $jenis = $_POST['jenis'];
        $jadwal = $_POST['jadwal'];
        $waktu = $_POST['waktu'];
        $query = "insert into data_dokter (nama,jenis,jadwal,waktu) values ('$nama','$jenis','$jadwal','$waktu')";
        $tambahData = mysqli_query($conn,$query);

        if ($tambahData == 'true') {
            $msg = "<p class='alert alert-success'>Data berhasil ditambahkan</p>";
            header("Location: admin/data-dokter.php?id=$id&msg=$msg");
        } 
        else {
            $msg = "<p class='alert alert-danger'>Data gagal ditambahkan</p>".mysqli_error($conn);
            header("Location: admin/data-dokter.php?id=$id&msg=$msg");
        }
    }
    // tambah data login
    else if (isset($_POST['add-data-login'])) {
        $id = $_POST['id'];
        $username = $_POST['username'];
        $nama = $_POST['nama'];
        $password1 = $_POST['password1'];
        $password2 = $_POST['password2'];
        if ($password1 != $password2) {
            $msg = "<p class='alert alert-danger'>Registrasi gagal. Password yang dimasukkan tidak sama</p>".mysqli_error($conn);
            header("Location: admin/data-pengguna.php?id=$id&msg=$msg");
        }
        else {
            $password = password_hash($password, PASSWORD_DEFAULT);
            $level = $_POST['level'];
            $query = "insert into data_login (username,nama,password,level) values ('$username','$nama','$password','$level')";
            $tambahData = mysqli_query($conn,$query);
    
            if ($tambahData == 'true') {
                $msg = "<p class='alert alert-success'>Data berhasil ditambahkan</p>";
                header("Location: admin/data-pengguna.php?id=$id&msg=$msg");
            } 
            else {
                $msg = "<p class='alert alert-danger'>Data gagal ditambahkan</p>".mysqli_error($conn);
                header("Location: admin/data-pengguna.php?id=$id&msg=$msg");
            }
        }
    }
    // buka sesi antrian oleh admin
    else if (isset($_POST['buka-sesi'])) {
        $id = $_POST['id'];
        $jadwal = $_POST['jadwal'];
        $waktu = $_POST['waktu'];
        $kuota = $_POST['kuota'];
        $sisa = $_POST['sisa'];
        $query = "insert into data_jadwal (jadwal,waktu,kuota,sisa) values ('$jadwal','$waktu','$kuota','$sisa')";
        $tambahData = mysqli_query($conn,$query);

        if ($tambahData == 'true') {
            $msg = "<p class='alert alert-success'>Antrian berhasil ditambahkan</p>";
            header("Location: admin/buka-sesi.php?id=$id&msg=$msg");
        } 
        else {
            $msg = "<p class='alert alert-danger'>Data gagal ditambahkan</p>".mysqli_error($conn);
            header("Location: admin/buka-sesi.php?id=$id&msg=$msg");
        }
    }
    // pendaftaran antrian oleh user
    else if (isset($_POST['pilih-jadwal'])) {
        $id = $_POST['id'];
        $nama = $_POST['nama'];
        $idJadwal = $_POST['idJadwal'];
        $klinik = $_POST['klinik'];
        $queryVal = "SELECT * FROM data_validasi WHERE idUser=$id";
        $resultVal = mysqli_query($conn,$queryVal);
        $dataVal = mysqli_fetch_assoc($resultVal);
        if (isset($dataVal["idUser"])) {
            $msg = "<p class='alert alert-danger'>Maaf, anda sudah memiliki antrian. Silahkan tunggu validasi dari Admin</p>";
            header("Location: user/pendaftaran-antrian.php?id=$id&msg=$msg");
        }
        else {
            $queryKuota = "SELECT * FROM data_jadwal WHERE id=$idJadwal";
            $resultKuota = mysqli_query($conn,$queryKuota);
            $dataKuota = mysqli_fetch_assoc($resultKuota);
            $jadwal = $dataKuota["jadwal"];
            $waktu = $dataKuota["waktu"];
            $kuota = $dataKuota["kuota"];
            $sisa = $dataKuota["sisa"];
            if ($sisa>0) {
                $sisaKuota = $sisa - 1;
                $query = "insert into data_validasi (idUser,idJadwal,nama,jadwal,waktu,klinik,kuota,sisa) values ('$id','$idJadwal','$nama','$jadwal','$waktu','$klinik','$kuota','$sisaKuota')";
                $tambahData = mysqli_query($conn,$query);
                if ($tambahData == 'true') {
                    $querySisa = "UPDATE data_jadwal SET sisa='$sisaKuota' WHERE id='$idJadwal'";
                    mysqli_query ($conn,$querySisa);
                    $msg = "<p class='alert alert-success'>Antrian berhasil ditambahkan</p>";
                    header("Location: user/pendaftaran-antrian.php?id=$id&msg=$msg");
                } 
                else {
                    $msg = "<p class='alert alert-danger'>Antrian gagal ditambahkan</p>".mysqli_error($conn);
                    header("Location: user/pendaftaran-antrian.php?id=$id&msg=$msg");
                }
            }
            else {
                $msg = "<p class='alert alert-danger'>Maaf, kuota antrian telah penuh.</p>";
                header("Location: user/pendaftaran-antrian.php?id=$id&msg=$msg");
            }
        }
    }
    // validasi antrian oleh admin
    else if (isset($_POST['validasi'])) {
        $idSidebar = $_POST['idSidebar'];
        $idUser = $_POST['idUser'];
        $id = $_POST['id'];
        $jadwal = $_POST['jadwal'];
        $waktu = $_POST['waktu'];
        $nama = $_POST['nama'];
        $klinik = $_POST['klinik'];
        $nomorAntrian = $_POST['nomorAntrian'];
        $query = "insert into data_antrian (idUser,jadwal,waktu,nama,klinik,nomor) values ('$idUser','$jadwal','$waktu','$nama','$klinik','$nomorAntrian')";
        $inputNomor = mysqli_query($conn,$query);

        if ($inputNomor == 'true') {
            $queryAntrian = "delete from data_validasi where id=$id";
            mysqli_query($conn,$queryAntrian);
            $msg = "<p class='alert alert-success'>Antrian berhasil di validasi</p>";
            header("Location: admin/validasi.php?id=$idSidebar&msg=$msg");
        } 
        else {
            $msg = "<p class='alert alert-danger'>Data gagal di validasi</p>".mysqli_error($conn);
            header("Location: admin/validasi.php?id=$idSidebar&msg=$msg");
        }
    }
    // hapus data validasi oleh admin
    else if (isset($_GET['delValidasi'])) {
        $idSidebar = $_GET['idSidebar'];
        $idVal = $_GET['idVal'];
        $idJadwal = $_GET['idJadwal'];
        $query = "delete from data_validasi where id=$idVal";
        if (mysqli_query ($conn,$query) == 'true') {
            $queryKuota = "SELECT * FROM data_jadwal WHERE id=$idJadwal";
            $resultKuota = mysqli_query($conn,$queryKuota);
            $dataKuota = mysqli_fetch_assoc($resultKuota);
            $sisa = $dataKuota["sisa"];
            $sisaKuota = $sisa + 1;
            $querySisa = "UPDATE data_jadwal SET sisa='$sisaKuota' WHERE id='$idJadwal'";
            mysqli_query ($conn,$querySisa);
            $msg = "<p class='alert alert-success'>Data validasi berhasil dihapus</p>";
            header("Location: admin/validasi.php?id=$idSidebar&msg=$msg");
        } 
        else {
            $msg = "<p class='alert alert-danger'>Data validasi gagal dihapus</p>".mysqli_error($conn);
            header("Location: admin/validasi.php?id=$idSidebar&msg=$msg");
        }
    }
    // hapus data status antrian oleh user
    else if (isset($_GET['batal'])) {
        $idSidebar = $_GET['idSidebar'];
        $idVal = $_GET['idVal'];
        $idJadwal = $_GET['idJadwal'];
        $query = "delete from data_validasi where id=$idVal";
        if (mysqli_query ($conn,$query) == 'true') {
            $queryKuota = "SELECT * FROM data_jadwal WHERE id=$idJadwal";
            $resultKuota = mysqli_query($conn,$queryKuota);
            $dataKuota = mysqli_fetch_assoc($resultKuota);
            $sisa = $dataKuota["sisa"];
            $sisaKuota = $sisa + 1;
            $querySisa = "UPDATE data_jadwal SET sisa='$sisaKuota' WHERE id='$idJadwal'";
            mysqli_query ($conn,$querySisa);
            $msg = "<p class='alert alert-success'>Antrian berhasil dibatalkan</p>";
            header("Location: user/status-antrian.php?id=$idSidebar&msg=$msg");
        } 
        else {
            $msg = "<p class='alert alert-danger'>Antrian gagal dibatalkan</p>".mysqli_error($conn);
            header("Location: user/status-antrian.php?id=$idSidebar&msg=$msg");
        }
    }
    // hapus data sesi antrian oleh admin
    else if (isset($_GET['delSesi'])) {
        $idSidebar = $_GET['idSidebar'];
        $id = $_GET['id'];
        $query = "delete from data_jadwal where id=$id";
    
        if (mysqli_query ($conn,$query) == 'true') {
            $msg = "<p class='alert alert-success'>Data sesi antrian berhasil dihapus</p>";
            header("Location: admin/buka-sesi.php?id=$idSidebar&msg=$msg");
        } 
        else {
            $msg = "<p class='alert alert-danger'>Data sesi antrian gagal dihapus</p>".mysqli_error($conn);
            header("Location: admin/buka-sesi.php?id=$idSidebar&msg=$msg");
        }
    }
    // tombol antrian selanjutnya
    else if (isset($_POST['btn-nextQ'])) {
        $idSidebar = $_POST['idSidebar'];
        $nextQ = $_POST['nextQ'];
        $query = "delete from data_antrian where nomor=$nextQ";
    
        if (mysqli_query ($conn,$query) == 'true') {
            header("Location: admin/index.php?id=$idSidebar");
        } 
        else {
            header("Location: admin/index.php?id=$idSidebar");
        }
    }
    // pengisisan data diri oleh user
    else if (isset($_POST['data-diri'])) {
        $idSidebar = $_POST['idSidebar'];
        $nama = $_POST['nama'];
        $ttlTempat = $_POST['ttlTempat'];
        $ttlTanggal = $_POST['ttlTanggal'];
        $gender = $_POST['gender'];
        $kewarganegaraan = $_POST['kewarganegaraan'];
        $agama = $_POST['agama'];
        $pekerjaan = $_POST['pekerjaan'];
        $pendidikan = $_POST['pendidikan'];
        $asuransi = $_POST['asuransi'];
        $alamat = $_POST['alamat'];
        $noHP = $_POST['noHP'];
        $email = $_POST['email'];
        $namaWali = $_POST['namaWali'];
        $hubunganWali = $_POST['hubunganWali'];
        $alamatWali = $_POST['alamatWali'];
        $noHPwali = $_POST['noHPwali'];
      
        $query = "INSERT INTO data_diri (id,nama,ttlTempat,ttlTanggal,gender,kewarganegaraan,agama,pekerjaan,pendidikan,asuransi,alamat,noHP,email,namaWali,hubunganWali,alamatWali,noHPwali) VALUES ('$idSidebar','$nama','$ttlTempat','$ttlTanggal','$gender','$kewarganegaraan','$agama','$pekerjaan','$pendidikan','$asuransi','$alamat','$noHP','$email','$namaWali','$hubunganWali','$alamatWali','$noHPwali')";
        $isiData = mysqli_query ($conn,$query);
        if ($isiData == 'true') {
            $msg = "<p class='alert alert-success'>Data diri berhasil ditambahkan</p>";
            header("Location: user/data-diri.php?id=$idSidebar&msg=$msg");
        } 
        else {
            $msg = "<p class='alert alert-danger'>Data diri gagal ditambahkan</p>".mysqli_error($conn);
            header("Location: user/data-diri.php?id=$idSidebar&msg=$msg");
        }
    }
    // ubah data diri oleh user
    else if (isset($_POST['edit-data-diri'])) {
        $idSidebar = $_POST['idSidebar'];
        $nama = $_POST['nama'];
        $ttlTempat = $_POST['ttlTempat'];
        $ttlTanggal = $_POST['ttlTanggal'];
        $gender = $_POST['gender'];
        $kewarganegaraan = $_POST['kewarganegaraan'];
        $agama = $_POST['agama'];
        $pekerjaan = $_POST['pekerjaan'];
        $pendidikan = $_POST['pendidikan'];
        $asuransi = $_POST['asuransi'];
        $alamat = $_POST['alamat'];
        $noHP = $_POST['noHP'];
        $email = $_POST['email'];
        $namaWali = $_POST['namaWali'];
        $hubunganWali = $_POST['hubunganWali'];
        $alamatWali = $_POST['alamatWali'];
        $noHPwali = $_POST['noHPwali'];
        $query = "update data_diri set nama='$nama',ttlTempat='$ttlTempat',ttlTanggal='$ttlTanggal',gender='$gender',kewarganegaraan='$kewarganegaraan',agama='$agama',pendidikan='$pendidikan',asuransi='$asuransi',alamat='$alamat',noHP='$noHP',email='$email',namaWali='$namaWali',hubunganWali='$hubunganWali',alamatWali='$alamatWali',noHPwali='$noHPwali' where id=$idSidebar";
        $editData = mysqli_query ($conn,$query);
        if ($editData == 'true') {
            $msg = "<p class='alert alert-success'>Data diri berhasil di ubah</p>";
            header("Location: user/data-diri.php?id=$idSidebar&msg=$msg");
        } 
        else {
            $msg = "<p class='alert alert-danger'>Data diri gagal di ubah</p>".mysqli_error($conn);
            header("Location: user/data-diri.php?id=$idSidebar&msg=$msg");
        }
    }
    // reset password data login user
    else if (isset($_POST['reset'])) {
        $usernameReset = $_POST['username'];
        $passwordBaru1 = $_POST['password1'];
        $passwordBaru2 = $_POST['password2'];
        $querySelect = "SELECT * FROM data_login WHERE username='$usernameReset'";
        $resetSelect = mysqli_query ($conn,$querySelect);
        $resultSelect = mysqli_fetch_assoc($resetSelect);
        $level = $resultSelect ['level'];
        if ($level == 1) {
            $msg = "<p class='alert alert-danger'>Tidak dapat merubah password Admin</p>";
            header("Location: reset-password.php?msg=$msg");
        }
        else {
            if ($passwordBaru1 != $passwordBaru2) {
                $msg = "<p class='alert alert-danger'>Reset password gagal. Password yang dimasukkan tidak sama</p>".mysqli_error($conn);
                header("Location: reset-password.php?msg=$msg");
            }
            else {
                $passwordBaru = password_hash($passwordBaru1, PASSWORD_DEFAULT);
                $query = "UPDATE data_login SET password='$passwordBaru' WHERE username='$usernameReset'";
                $reset = mysqli_query ($conn,$query);
                if (mysqli_query ($conn,$query) == 'true') {
                    $msg = "<p class='alert alert-success'>Reset password berhasil</p>";
                    header("Location: index.php?msg=$msg");
                } 
                else {
                    $msg = "<p class='alert alert-success'>Reset password gagal</p>";
                    header("Location: reset-password.php?msg=$msg");
                }
            }
        }
    }
?>